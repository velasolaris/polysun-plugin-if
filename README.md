# polysun-plugin-if


### Latest release
[![Maven Central](https://img.shields.io/maven-central/v/com.velasolaris.polysun/polysun-plugin-if.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.velasolaris.polysun%22%20AND%20a:%22polysun-plugin-if%22)

Polysun Plugin Interfaces
=========================

Recommended: Base your project on the [`polysun-plugin-template`](https://bitbucket.org/velasolaris/polysun-plugin-template).

This project contains the interfaces required for Polysun plugin controller development.
The plugin interfaces are managed by Vela Solaris AG. Changing them will break compatibility of your plugins with Polysun.

Classes to get started with:

- [`IPluginController`](./src/main/java/com/velasolaris/plugin/controller/spi/IPluginController.java): Interface for plugin controllers
- [`AbstractPluginController`](./src/main/java/com/velasolaris/plugin/controller/spi/AbstractPluginController.java): Abstract class for plugin controllers that implements the `IPluginController` interface
- [`ControllerPlugin`](./src/main/java/com/velasolaris/plugin/controller/spi/ControllerPlugin.java): Interface for plugin detection
- [`AbstractControllerPlugin`](./src/main/java/com/velasolaris/plugin/controller/spi/AbstractControllerPlugin.java): Abstract class with the default implementations for detecting plugins.
- [`PluginControllerConfiguration`](./src/main/java/com/velasolaris/plugin/controller/spi/PluginControllerConfiguration.java): Configuration of plugin controllers, used to tell Polysun about sensor inputs, controller outputs and other properties.
- [`IPluginImagePainter`](./src/main/java/com/velasolaris/plugin/controller/spi/IPluginImagePainter.java): Interface for painting plugin images. A custom image painter can be provided by overriding the `PluginControllerConfiguration#getImagePainter()` method.
                       An image painter could load images from a PNG file, or it could paint SVG images that have been transcoded using [Radiance/Photon](https://github.com/kirill-grouchnikov/radiance/blob/master/docs/photon/photon.md).

The following projects are open source and can be referred to as examples for plugin development:

- [polysun-public-plugin](https://bitbucket.org/velasolaris/polysun-public-plugin/)
- [polysun-demo-plugin](https://bitbucket.org/velasolaris/polysun-demo-plugin/)

Additionally, please refer to the [API documentation](https://javadoc.io/doc/com.velasolaris.polysun/polysun-plugin-if/).
This code is Open Source, see [LICENSE.txt](https://bitbucket.org/velasolaris/polysun-plugin-if/src/master/LICENSE.txt). It can be used to create new plugin controllers for use in Polysun.

So that Polysun can find a controller plugin, the class implementation has to be added to the following file:
`src/main/resources/META-INF/services/com.velasolaris.plugin.controller.spi.ControllerPlugin`.
See the [file in the polysun-demo-plugin template](https://bitbucket.org/velasolaris/polysun-demo-plugin/src/master/src/main/resources/META-INF/services/com.velasolaris.plugin.controller.spi.ControllerPlugin) for an example.

Requirements for controller plugin development
----------------------------------------------

- [JDK 17](https://bell-sw.com/pages/downloads/#/java-17-current)

Build
-----

- Linux/Mac: `./gradlew build`
- Windows: `gradlew.bat build`

Development
-----------

- To import the project into Eclipse, use File > Import... > Gradle > Existing Gradle Project.
- In IntelliJ IDEA, use File > Open... > Select the `build.gradle.kts` file.

Debugging
---------

As it is not possible to use breakpoints when running a plugin from Polysun, it is recommended to use the `java.util.logging.Logger`.

