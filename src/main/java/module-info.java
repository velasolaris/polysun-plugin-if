module com.velasolaris.polysun.plugin.api {
    requires java.logging;
    requires org.apache.commons.io;
    requires transitive java.desktop;
    exports com.velasolaris.plugin;
    exports com.velasolaris.plugin.controller;
    exports com.velasolaris.plugin.controller.spi;
}
