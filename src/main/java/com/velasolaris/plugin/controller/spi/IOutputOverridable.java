package com.velasolaris.plugin.controller.spi;

/**
 * Interface that makes it possible to override the output of a controller using another controller.
 * In some cases, it may be necessary to perform a control operation with one controller first (e.g., a Heating Controller)
 * and then either pass on its output or override it with another controller. This interface provides the methods necessary for performing
 * controller output overrides.
 * It can be used to maximize code reuse by extending the capabilities of Polysun's built-in controllers without having to re-engineer them again.
 * Any controller that has control signals can be added. The controllers are added at the beginning of the simulation, so they should only be accessed
 * in the {@code initialiseSimulation()} and {@code control()} methods.
 * @author Marc Jakobi
 * @since Polysun 11.0
 * @see AbstractPluginController
 * @see AbstractPluginController#getOverridableController(String)
 * @see IPluginController
 */
public interface IOutputOverridable {

    /**
     * Set this method to always return <code>false</code> if the controller does not implement methods having its control signals overridden
     * and if it should not be added to the list of overridable controllers.
     * @return <code>true</code> if the controller's output can be overriden.
     */
    boolean isOverridable();

    /**
     * This method is used to recognize the controller.
     * @return A String of what the user entered into the controller GUI's "Description" field.
     */
    String getUserDescription();

    /**
     * @return An array of the controller's control signals. They are returned in the order of the controller's outputs in the GUI.
     */
    float[] getControlSignals();

    /**
     * Overwrites the controller's control signals with the values passed in the controlSignals array.
     * @param controlSignals values to overwrite this controller's control singals with, in the order of this controller's control signals.
     */
    void overrideControlSignals(float[] controlSignals) throws PluginControllerException;
}
