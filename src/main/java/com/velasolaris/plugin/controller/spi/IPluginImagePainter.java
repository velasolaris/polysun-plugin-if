package com.velasolaris.plugin.controller.spi;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

import static java.awt.Color.WHITE;

/**
 * Interface for painting plugin images in the Polysun designer.
 *
 */
public interface IPluginImagePainter {

    /** The default width of the plugin image in px */
    public static final int DEFAULT_WIDTH = 37;

    /** The default height of the plugin image in px */
    public static final int DEFAULT_HEIGHT = 23;

    /**
     * Paints the plugin image.
     * The default implementation paints an SVG image transcoded using Radiance/Photon:
     * https://github.com/kirill-grouchnikov/radiance/blob/master/docs/photon/photon.md
     * @param g the graohics context.
     */
    default void paint(Graphics2D g) {
        Shape shape = null;

        java.util.LinkedList<AffineTransform> transformations = new java.util.LinkedList<AffineTransform>();


        //

        // _0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 31.072315f, -38.340137f));

        // _0_0

        g.setTransform(transformations.pop()); // _0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.90677965f, 0, 0, 0.90677965f, 12.552985f, -38.65011f));

        // _0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 19.028038f, 1.4636421f));

        // _0_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.39790422f, 0, 0, 0.39790422f, 0.83772373f, 1.7224818f));

        // _0_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 30.69656f, -0.72493494f));

        // _0_1_0_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0, 0.971383f, -0.9713829f, 0, -13.19697f, 62.049095f));

        // _0_1_0_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 4.5428276f, -0.255567f));

        // _0_1_0_0_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.7497474f, 0, 0, 0.7509831f, -82.78321f, 65.3706f));

        // _0_1_0_0_0_1_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -90.646484f, 70.39424f));

        // _0_1_0_0_0_1_1

        // _0_1_0_0_0_1_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 90.646484f, -70.39424f));

        // _0_1_0_0_0_1_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0.01350987f, 0.02869167f));

        // _0_1_0_0_0_1_1_0_0_0

        // _0_1_0_0_0_1_1_0_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-119.9707, 100.06445);
        ((GeneralPath) shape).lineTo(-119.9707, 102.841805);
        ((GeneralPath) shape).lineTo(-119.9707, 105.623055);
        ((GeneralPath) shape).lineTo(-119.9707, 163.81055);
        ((GeneralPath) shape).lineTo(-17.423828, 163.81055);
        ((GeneralPath) shape).lineTo(-17.423828, 100.064445);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-114.41406, 105.62109);
        ((GeneralPath) shape).lineTo(-22.978516, 105.62109);
        ((GeneralPath) shape).lineTo(-22.978516, 105.62309);
        ((GeneralPath) shape).lineTo(-114.41406, 105.62309);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-108.85938, 111.179695);
        ((GeneralPath) shape).lineTo(-28.535156, 111.179695);
        ((GeneralPath) shape).lineTo(-28.535156, 152.69531);
        ((GeneralPath) shape).lineTo(-108.85938, 152.69531);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-114.41406, 158.25195);
        ((GeneralPath) shape).lineTo(-22.978516, 158.25195);
        ((GeneralPath) shape).lineTo(-22.978516, 158.25395);
        ((GeneralPath) shape).lineTo(-114.41406, 158.25395);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-117.19177, 102.8426);
        ((GeneralPath) shape).lineTo(-117.19177, 105.62262);
        ((GeneralPath) shape).lineTo(-117.19177, 161.03162);
        ((GeneralPath) shape).lineTo(-20.20134, 161.03162);
        ((GeneralPath) shape).lineTo(-20.20134, 102.8426);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-111.63803, 108.40069);
        ((GeneralPath) shape).lineTo(-25.757027, 108.40069);
        ((GeneralPath) shape).lineTo(-25.757027, 155.47351);
        ((GeneralPath) shape).lineTo(-111.63803, 155.47351);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-114.41278, 105.63626);
        ((GeneralPath) shape).lineTo(-22.929024, 105.63626);
        ((GeneralPath) shape).lineTo(-22.929024, 158.25241);
        ((GeneralPath) shape).lineTo(-114.41278, 158.25241);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0xEEEEEE));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -4.157296f, -67.20962f));

        // _0_1_0_0_0_1_1_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 94.80379f, -3.184622f));

        // _0_1_0_0_0_1_1_0_1_0
        shape = new Rectangle2D.Double(-108.8812026977539, 111.16268157958984, 63.86603927612305, 41.526248931884766);
        g.setPaint(WHITE);
        g.fill(shape);
        g.setPaint(new Color(0x323232));
        g.setStroke(new BasicStroke(5.5303283f, 2, 0, 4));
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_0

        // _0_1_0_0_0_1_1_0_1_1
        shape = new Rectangle2D.Double(-14.07741928100586, 107.97805786132812, 63.86603927612305, 41.526248931884766);
        g.setPaint(WHITE);
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(-0.38680756f, 3.6438565f, -3.6263845f, -0.3942753f, 102.33083f, 138.82184f));

        // _0_1_0_0_0_1_1_0_1_2

        // _0_1_0_0_0_1_1_0_1_2_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.0148706, 19.43818);
        ((GeneralPath) shape).lineTo(3.4958706, 17.919182);
        ((GeneralPath) shape).lineTo(2.6678705, 17.919182);
        ((GeneralPath) shape).lineTo(1.2148706, 19.37218);
        ((GeneralPath) shape).lineTo(0.2808705, 18.43818);
        ((GeneralPath) shape).lineTo(-1.5851295, 20.305182);
        ((GeneralPath) shape).lineTo(-2.5181296, 19.37218);
        ((GeneralPath) shape).lineTo(-4.86613, 21.719181);
        ((GeneralPath) shape).lineTo(-3.9321299, 22.652182);
        ((GeneralPath) shape).lineTo(-5.25813, 23.978182);
        ((GeneralPath) shape).curveTo(-5.6761303, 24.396181, -5.8081303, 25.014181, -5.65613, 25.567183);
        ((GeneralPath) shape).curveTo(-3.5533812, 29.319574, 1.6281719, 22.928421, -3.879869, 25.223953);
        ((GeneralPath) shape).curveTo(-4.6547194, 28.609463, -2.1928124, 29.145876, -1.0441298, 28.192183);
        ((GeneralPath) shape).lineTo(0.28187013, 26.866182);
        ((GeneralPath) shape).lineTo(1.2148702, 27.801182);
        ((GeneralPath) shape).lineTo(3.56187, 25.452183);
        ((GeneralPath) shape).lineTo(2.62887, 24.519182);
        ((GeneralPath) shape).lineTo(4.49487, 22.652182);
        ((GeneralPath) shape).lineTo(3.56187, 21.719181);
        ((GeneralPath) shape).lineTo(5.01487, 20.266182);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_0_0_0_1_1_0_1_2_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.4521291, 21.719181);
        ((GeneralPath) shape).lineTo(-2.519129, 20.78618);
        ((GeneralPath) shape).lineTo(-1.585129, 21.719181);
        ((GeneralPath) shape).lineTo(0.28187096, 19.85218);
        ((GeneralPath) shape).lineTo(1.214871, 20.78618);
        ((GeneralPath) shape).lineTo(3.081871, 18.91918);
        ((GeneralPath) shape).lineTo(4.014871, 19.85218);
        ((GeneralPath) shape).lineTo(2.147871, 21.719181);
        ((GeneralPath) shape).lineTo(3.080871, 22.652182);
        ((GeneralPath) shape).lineTo(1.214871, 24.519182);
        ((GeneralPath) shape).lineTo(2.147871, 25.452183);
        ((GeneralPath) shape).lineTo(1.2148709, 26.386183);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-2.6851292, 26.552181);
        ((GeneralPath) shape).curveTo(-2.910129, 26.778181, -2.9121292, 27.20618, -2.6581292, 27.45918);
        ((GeneralPath) shape).curveTo(-2.4041293, 27.712181, -1.9771292, 27.711182, -1.7511292, 27.48518);
        ((GeneralPath) shape).lineTo(-0.65112925, 26.38518);
        ((GeneralPath) shape).lineTo(-1.5851293, 25.452179);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-4.5511293, 24.68518);
        ((GeneralPath) shape).curveTo(-4.777129, 24.91118, -4.7781296, 25.33918, -4.5251293, 25.59218);
        ((GeneralPath) shape).curveTo(-4.2721295, 25.84618, -3.8441293, 25.844181, -3.6181293, 25.61918);
        ((GeneralPath) shape).lineTo(-2.5181293, 24.51918);
        ((GeneralPath) shape).lineTo(-3.4521294, 23.58618);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00BF77));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_2

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 90.24797f, -53.794605f));

        // _0_1_0_0_0_1_1_0_2
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0.45535448f, 0));

        // _0_1_0_0_0_1_1_0_2_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -1.0588328E-6f, -16.602507f));

        // _0_1_0_0_0_1_1_0_2_0_0

        // _0_1_0_0_0_1_1_0_2_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_2_0_0_1
        shape = new Rectangle2D.Double(-36.81454086303711, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -1.0588328E-6f, 16.60251f));

        // _0_1_0_0_0_1_1_0_2_0_1

        // _0_1_0_0_0_1_1_0_2_0_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_2_0_1_1
        shape = new Rectangle2D.Double(-36.81454086303711, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2_0_1

        // _0_1_0_0_0_1_1_0_2_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2_0

        // _0_1_0_0_0_1_1_0_2_1
        shape = new Rectangle2D.Double(-36.359188079833984, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0

        g.setTransform(transformations.pop()); // _0_1_0

        g.setTransform(transformations.pop()); // _0_1

    }

    /**
     * @return the width of the plugin image in px
     */
    default int getWidth() {
        return DEFAULT_WIDTH;
    }

    /**
     * @return the height of the plugin image in px
     */
    default int getHeight() {
        return DEFAULT_HEIGHT;
    }
}
