package com.velasolaris.plugin.controller.spi;

/**
 * Exception signaling a problem with a plugin controller.
 * See chained exception for the root cause.
 *
 * @author Roland Kurmann
 * @since Polysun 9.1
 *
 */
public class PluginControllerException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor without any information about the cause.
     */
    public PluginControllerException() {
    }

    /**
     * Constructor.
     * @param aMessage an error message
     */
    public PluginControllerException(String aMessage) {
        super(aMessage);
    }

    /**
     * Constructor.
     * @param aCause the {@code Throwable} that caused this {@code PluginControllerException} to be thrown
     */
    public PluginControllerException(Throwable aCause) {
        super(aCause);
    }

    /**
     * Constructor.
     * @param aMessage an error message
     * @param aCause the {@code Throwable} that caused this {@code PluginControllerException} to be thrown
     */
    public PluginControllerException(String aMessage, Throwable aCause) {
        super(aMessage, aCause);
    }

    /**
     * Constructor
     * @param aMessage an error message
     * @param aCause the {@code Throwable} that caused this {@code PluginControllerException} to be thrown
     * @param aEnableSuppression whether on not suppression is enabled or disabled
     * @param aWritableStackTrace whether or not the stack trace should be writable
     */
    public PluginControllerException(String aMessage, Throwable aCause, boolean aEnableSuppression, boolean aWritableStackTrace) {
        super(aMessage, aCause, aEnableSuppression, aWritableStackTrace);
    }

}
