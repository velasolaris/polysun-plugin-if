/**
 *
 */
package com.velasolaris.plugin.controller.spi;

import java.util.List;

/**
 * Interface for Polysun components that can be pre-simulated.
 * Coponents in Polysun that can be pre-simulated implement this interface.
 * Instances of the components can be extracted using the PluginController's getPreSimulatableComponent() method.
 * <br><br>
 * The only method defined by this interface is {@link #preSimulate(List)}. It takes a <code>List</code> of <code>Object</code>s as an input
 * and returns a <code>List</code> of a subclass of an <code>Object</code>.
 * The currently available components and their parameters are listed in the following table. It will be updated as components are added.
 * <table border="1">
 * <tr>
 *   <th> Component key </th> <th> return type </th> <th> Input arguments </th>
 * </tr>
 * <tr>
 *   <td> BATTERY </td> <td> List of type <code>Double</code> </td> <td> List of type <code>Object</code> with contents: <br>
 *   <code>int</code>: Simulation time since 01.01. 00:00:00 [s] <br>
 *   <code>double</code>: Charging/discharging power [kW] (positive power means battery charge, negative power means battery discharge) <br>
 *   <code>int</code>: Length of the pre-simulated timestep [s]
 *   </td>
 * </tr>
 * </table>
 * </p>
 * @author Marc Jakobi, 07.27.2017
 * @see IPluginController
 */
public interface IPreSimulatable {

    /**
     * Pre-simulates one or more time steps of the component.
     * @param args A <code>List</code> of input arguments
     * @return A <code>List</code> containing the results of the pre-simulation.
     */
    public List<Object> preSimulate(List<Object> args);
}
