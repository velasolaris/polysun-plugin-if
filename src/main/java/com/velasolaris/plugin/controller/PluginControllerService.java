package com.velasolaris.plugin.controller;

import com.velasolaris.plugin.WildcardClassLoader;
import com.velasolaris.plugin.controller.spi.ControllerPlugin;
import com.velasolaris.plugin.controller.spi.IPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerException;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <b>This classloader is used by Polysun and is not part of the API.</b>
 * Singleton service returning all {@code PluginController}s that are available through {@code ControllerPlugin}s.
 *
 * Reference:
 * <ul>
 * 	<li>https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/ServiceLoader.html
 * 	<li>https://docs.oracle.com/javase/tutorial/ext/basics/spi.html
 * 	<li>http://stackoverflow.com/questions/465099/best-way-to-build-a-plugin-system-with-java
 * </ul>
 *
 * @see IPluginController
 * @see ControllerPlugin
 * @see PluginControllerService

 * @author Roland Kurmann
 * @author Marc Jakobi
 * @since Polysun 9.1
 *
 */
public class PluginControllerService {

    public static final int SERIALIZE_PLUGINS_INDEX = 0;
    public static final int DONT_SERIALIZE_PLUGINS_INDEX = 0;

    /** Static instance of the Logger for this class */
    protected static Logger sLog = Logger.getLogger(PluginControllerService.class.getName());

    /** Singleton. */
    private static PluginControllerService service;

    /** Root path to the plugins. Setting in init(). */
    private static String sRootPath;
    /** Default classpath to the plugins, e.g. "*.jar" relative to rootPath. Setting in init(). */
    private static String sDefaultClasspathPattern;

    /** Flag to determine whether to serialize plugins (<tt>true</tt>) or not (<tt>false</tt>). */
    private static boolean sSerializePlugins = true;

    /** Java Service loader to dynamically load controller plugins. */
    private ServiceLoader<ControllerPlugin> mControllerPluginLoader;

    /** The ClassLoader to use the plugins. */
    private WildcardClassLoader mClassLoader;

    /** Controllers cache. */
    private List<PluginControllerDefinition> mControllerDefinitionList;

    /**
     * Returns the singleton.
     * @return the singleton
     */
    public static synchronized PluginControllerService getInstance() {
        if (service == null) {
            service = new PluginControllerService();
        }
        return service;
    }

    /**
     * Init method set settings fo the singleton, i.e. the root path where the plugins should be loaded from.
     * @param rootPath the root path where the plugins should be loaded from
     */
    public static synchronized void init(String rootPath) {
        PluginControllerService.sRootPath = rootPath;
    }

    /**
     * Init method set settings fo the singleton, i.e. the root path where the plugins should be loaded from.
     * @param rootPath the root path where the plugins should be loaded from
     * @param defaultClasspathPattern the classpath to load at runtime the plugins, can contain *, e.g. "*.jar"
     *
     * @see WildcardClassLoader#addClassPath(String)
     */
    public static synchronized void init(String defaultClasspathPattern, String rootPath) {
        PluginControllerService.sRootPath = rootPath;
        PluginControllerService.sDefaultClasspathPattern = defaultClasspathPattern;
    }

    /** Hidden constructor for singleton. */
    private PluginControllerService() {
        initWildcardClassLoader();
        mControllerPluginLoader = ServiceLoader.load(ControllerPlugin.class, mClassLoader);
    }

    private void initWildcardClassLoader() {
        mClassLoader = new WildcardClassLoader(sDefaultClasspathPattern, sRootPath, getClass().getClassLoader());
    }

    /**
     * Loads all configured controller plugins and returns the enabled plugin controllers as a list.
     *
     * Not enabled plugins and controllers are ignored.
     *
     * @param reloadPlugins if <code>true</code>, plugins are looked up again in the classpath
     * @param parameters Generic parameters
     *
     * @return list of plugin controller definitions
     * @throws PluginControllerException For any problem with the automatic service loading
     */
    public List<PluginControllerDefinition> getPluginControllers(boolean reloadPlugins, Map<String, Object> parameters) throws PluginControllerException {
        try {
            if (reloadPlugins) {
                reloadPlugins();
            }
            if (mControllerDefinitionList == null) {
                mControllerDefinitionList = new ArrayList<>();
                for (ControllerPlugin plugin : mControllerPluginLoader) {
                    if (plugin.isEnabled(parameters)) {
                        List<Class<? extends IPluginController>> newControllers = plugin.getControllers(parameters);
                        if (newControllers != null) {
                            for (Class<? extends IPluginController> controller : newControllers) {
                                PluginControllerDefinition controllerDefinition = new PluginControllerDefinition(controller, plugin);
                                if (controllerDefinition.isEnabled(parameters)) {
                                    if (!mControllerDefinitionList.contains(controllerDefinition)) {
                                        mControllerDefinitionList.add(controllerDefinition);
                                    } else {
                                        sLog.warning("Plugin controllers with the same id " + controllerDefinition.getId() + ", first " + mControllerDefinitionList.get(mControllerDefinitionList.indexOf(controllerDefinition.getId())) + ", second " + controllerDefinition);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (mControllerDefinitionList.isEmpty()) {
                sLog.warning("No plugin controllers found.");
            }
            return Collections.unmodifiableList(mControllerDefinitionList);
        } catch (ServiceConfigurationError e) {
            throw new PluginControllerException("Could not load plugin controllers", e);
        }
    }

    private void reloadPlugins() {
        if (mClassLoader == null) {
            initWildcardClassLoader();
        }
        mClassLoader.reload();
        mControllerPluginLoader.reload();
        mControllerDefinitionList = null;
    }

    /**
     * Serializes the JAR file containing the specified ControllerPlugin
     * @param aPlugin the plugin to serialize.
     * @return a Map with the respective Files as keys and the serialized plugins as values.
     * @throws IOException
     */
    public Map<File, byte[]> serializePlugin(ControllerPlugin aPlugin) throws IOException {
        Map<File, byte[]> serializedFileMap = null;
        if (sSerializePlugins) {
            serializedFileMap = new HashMap<File, byte[]>();
            try {
                URL location = aPlugin.getClass().getProtectionDomain().getCodeSource().getLocation();
                File file = FileUtils.toFile(location);
                Path path = file.toPath();
                if (!path.toFile().isDirectory()) {
                    serializedFileMap.put(file, Files.readAllBytes(path));
                }
            } catch (AccessDeniedException e) {
                sLog.fine("Access denied to plugin (possibly a built-in plugin).");
            }
            if (serializedFileMap.isEmpty()) {
                serializedFileMap = null;
            }
        }
        return serializedFileMap;
    }

    /**
     * Saves the serialized plugin JAR to a temporary directory.
     * @param aFileMap
     * @throws PluginControllerException
     */
    public void saveSerializedPluginFile(Map<File, byte[]> aFileMap) throws PluginControllerException {
        try {
            File tempFile = Files.createTempDirectory("polysun.temorary.plugin").toFile();
            initWildcardClassLoaderWithTemporaryPath(tempFile.getAbsolutePath());
            getClassLoader().reload();
            for (Map.Entry<File, byte[]> entry : aFileMap.entrySet()) {
                File serializedFile = entry.getKey();
                FileOutputStream fos = new FileOutputStream(tempFile + File.separator + serializedFile.getName());
                fos.write(entry.getValue());
                fos.close();
            }
        } catch (IOException e) {
            sLog.severe("De-serialization of ControllerPlugin failed.");
            throw new PluginControllerException("Unable to deserialize controller plugin. " + e.getMessage(), e);
        }
    }

    private void initWildcardClassLoaderWithTemporaryPath(String aPath) {
        String tmp = sRootPath;
        PluginControllerService.init(aPath);
        initWildcardClassLoader();
        mControllerPluginLoader = ServiceLoader.load(ControllerPlugin.class, mClassLoader);
        PluginControllerService.init(tmp);
    }

    /**
     * Reloads a plugin controller definition.
     * This method must be called during deserialization of a variant.
     * Custom code must be refreshed.
     *
     * @param controllerToReload Plugin controller to reload
     * @param parameters Generic parameters
     * @param reloadPlugins if <code>true</code>, plugins are looked up again in the classpath
     * @return Refreshed controllerToReload definition
     * @throws PluginControllerException For any problem with the controller plugin reloading
     */
    public PluginControllerDefinition reload(PluginControllerDefinition controllerToReload, boolean reloadPlugins, Map<String, Object> parameters) throws PluginControllerException {
        List<PluginControllerDefinition> pluginControllers = getPluginControllers(reloadPlugins, parameters);
        for (PluginControllerDefinition controller : pluginControllers) {
            if (controller.equals(controllerToReload)) {
                controllerToReload.setControllerClass(controller.getControllerClass());
                controllerToReload.setPlugin(controller.getPlugin());
                return controllerToReload;
            }
        }
        return null;
    }

    /**
     * @return this instance's class loader
     */
    public WildcardClassLoader getClassLoader() {
        if (mClassLoader == null) {
            initWildcardClassLoader();
        }
        return mClassLoader;
    }

    /**
     * For testing purposes.
     * @param args command line arguments, they will be ignored
     * @throws PluginControllerException for plugin problems
     */
    public static void main(String... args) throws PluginControllerException {
        System.out.println("Available plugin controllers:");
        System.out.println("" + PluginControllerService.getInstance().getPluginControllers(false, null));
    }

    public void setSerializePlugins(int aSelectedIndex) {
        sSerializePlugins = aSelectedIndex == SERIALIZE_PLUGINS_INDEX;
    }

    public boolean isSerializePlugins() {
        return sSerializePlugins;
    }

    /**
     * Class representing a plugin controller.
     *
     * Classes and controllers are transient in order to avoid ClassNotFoundException.
     * Nothing of external classes must be serialized. All external classes must be reloaded at deserialization.
     *
     * @author rkurmann
     * @since Polysun 9.1
     *
     */
    public static class PluginControllerDefinition implements Serializable {

        private static final long serialVersionUID = 1L;

        /** Technical ID of the plugin controller, i.e. the class name of the plugin controller. */
        private String id;
        /** Store the plugin controller name in order to have it if there is deserialization problem. Reset after deserialization. */
        private String name;
        /** Store the plugin controller simple class name in order to have it if there is deserialization problem. Reset after deserialization. */
        private String technicalName;
        /** Map of serialized plugin files required to use this PluginController. They are added to the [pluginDataPath] upon de-serialization if the files do not exist. */
        private Map<File, byte[]> mSerializedPluginFileMap = null;

        /** Class of the plugin controller as returned by the plugin. */
        private transient Class<? extends IPluginController> mControllerClass;
        /** Private instance of the plugin controller in order to get name and other metadata. As well as to test instantiation. Reset after deserialization. */
        private transient IPluginController mController;
        /** Reference to the plugin where the plugin controller is contained. Reset after deserialization. */
        private transient ControllerPlugin mPlugin;
        /** Exception occurred during deserialization (reload of variant). Is <code>null</code> if no exception occurred. */
        private transient PluginControllerException mReloadException;

        /**
         * Constructor.
         *
         * @param pluginControllerClass Plugin controller
         * @param plugin Enclosing controller plugin
         */
        /*package private*/ PluginControllerDefinition(Class<? extends IPluginController> pluginControllerClass, ControllerPlugin plugin) throws PluginControllerException {
            id = pluginControllerClass.getName(); // Set only once at construction, do not reload the id
            technicalName = pluginControllerClass.getSimpleName(); // Set only once at construction, do not reload the technicalName
            setControllerClass(pluginControllerClass);
            setPlugin(plugin);
        }

        /**
         * Returns the technical ID of the plugin controller, i.e. the class name of the plugin controller.
         *
         * This id is used for equality tests.
         *
         * @return Technical id (String representation of the class name)
         */
        public String getId() {
            return id;
        }

        /**
         * Returns the name of plugin controller as specified by the plugin controller.
         * @return name of the controller
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the description of plugin controller as specified by the plugin controller.
         * @return description of the controller
         */
        public String getDescription() {
            if (mController.getDescription() == null) {
                return null;
            } else {
                return mController.getDescription();
            }
        }

        /**
         * Returns the technical name of plugin controller (simple class name)
         * @return technical name of the controller (simple class name)
         */
        public String getTechnicalName() {
            return technicalName;
        }

        /**
         * Is this plugin controller enabled?
         *
         * @param parameters Generic parameters
         * @return <code>true</code> if enabled, otherwise <code>false</code>
         */
        public boolean isEnabled(Map<String, Object> parameters) {
            return mController.isEnabled(parameters);
        }

        /**
         * Factory method to create a new plugin controller.
         *
         * @exception PluginControllerException For any creation problem
         * @return a new instance plugin controller instance
         */
        public IPluginController createPluginController() throws PluginControllerException {
            if (mReloadException != null) {
                throw mReloadException;
            }
            try {
                return mControllerClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new PluginControllerException("Could not create plugin controller: " + id, e);
            }
        }

        /**
         * Returns the plugin controller class.
         *
         * @return Plugin controller class, or <code>null</code> if not reloaded properly
         */
        public Class<? extends IPluginController> getControllerClass() {
            return mControllerClass;
        }

        /**
         * Get the enveloping plugin.
         *
         * @return Controller plugin, or <code>null</code> if not reloaded properly
         */
        public ControllerPlugin getPlugin() {
            return mPlugin;
        }

        /**
         * Returns the name of the jar which contains the plugin controller.
         *
         * @return the name or an empty string if plugin controller is not in a jar (e.g. run in Eclipse)
         */
        public String getPluginJarName() {
            if (mControllerClass != null) {
                String urlPath = mControllerClass.getResource(mControllerClass.getSimpleName() + ".class").toString();
                Matcher matcher = Pattern.compile("([^/]+)\\.jar!").matcher(urlPath);
                if (matcher.find()) {
                    return matcher.group(1);
                }
            }
            return "";
        }

        /**
         * Returns the name of the plugin which contains the plugin controller.
         *
         * @return the name or an empty string if not set
         */
        public String getPluginName() {
            if (mPlugin != null && mPlugin.getName() != null && !"".equals(mPlugin.getName())) {
                return mPlugin.getName();
            } else {
                return "";
            }
        }

        /**
         * Returns the description of the plugin which contains the plugin controller.
         *
         * @return the description or an empty string if not set
         */
        public String getPluginDescription() {
            if (mPlugin != null && mPlugin.getDescription() != null && !"".equals(mPlugin.getDescription())) {
                return mPlugin.getDescription();
            } else {
                return "";
            }
        }

        /**
         * Returns the description of the plugin which contains the plugin controller.
         *
         * @return the description or an empty string if not set
         */
        public String getPluginCreator() {
            if (mPlugin != null && mPlugin.getCreator() != null && !"".equals(mPlugin.getCreator())) {
                return mPlugin.getCreator();
            } else {
                return "";
            }
        }

        /**
         * Returns the name of the plugin which contains the plugin controller.
         *
         * @return the name or an empty string if not set
         */
        public String getPluginVersion() {
            if (mPlugin != null && mPlugin.getVersion() != null && !"".equals(mPlugin.getVersion())) {
                return mPlugin.getVersion();
            } else {
                return "";
            }
        }

        /**
         * Returns the supported interface version of the plugin which contains the plugin controller.
         *
         * @param parameters General parameters
         * @return 	<li> 1 for compatibility with Polysun 9.1 and above
         * 		   	<li> 2 for compatibility with Polysun 11.0 and above
         * 			<li> 3 for compatibility with Polysun 11.3 and above
         */
        public int getPluginSupportedInterfaceVersion(Map<String, Object> parameters) {
            if (mPlugin != null) {
                return mPlugin.getSupportedInterfaceVersion(parameters);
            } else {
                return -1;
            }
        }

        /**
         * Returns the supported interface version of the plugin which contains the plugin controller.
         *
         * @param parameters General parameters
         * @return 1 for Polysun 9.1 interface version
         */
        public boolean isPluginEnabled(Map<String, Object> parameters) {
            if (mPlugin != null) {
                return mPlugin.isEnabled(parameters);
            } else {
                return false;
            }
        }

        /**
         * Sets the controller class, creates a controller instance and updates stored fields.
         *
         * Used for reload.
         *
         * @param controllerClass controller class to set
         * @throws PluginControllerException For any problems while creating the controller instance
         */
        /*package private*/ void setControllerClass(Class<? extends IPluginController> controllerClass) throws PluginControllerException {
            this.mControllerClass = controllerClass;
            mController = createPluginController();
            name = mController.getName();
            if (name == null || "".equals(name)) {
                throw new PluginControllerException("Plugin controller name must not be empty or null");
            }
        }

        /**
         * Sets the reference to the enclosing plugin.
         *
         * @param plugin The plugin belonging to the plugin controller.
         */
        /*package private*/ void setPlugin(ControllerPlugin plugin) {
            this.mPlugin = plugin;
        }

        /**
         * Saves the plugin controller configuration in order to detect changes in the next reload.
         */
        private void writeObject(ObjectOutputStream s) throws IOException {
            if (null != mPlugin) {
                mSerializedPluginFileMap = PluginControllerService.getInstance().serializePlugin(mPlugin);
            }
            s.defaultWriteObject();
        }

        /**
         * De-serialization.
         * Reload controller plugin definition.
         *
         * @param aInputStream input stream to deserialize
         * @throws IOException for any I/O problems
         * @throws ClassNotFoundException for classes not found
         */
        private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
            aInputStream.defaultReadObject();

            try {
                if (PluginControllerService.getInstance().reload(this, false, null) == null) {
                    if (!deSerializeControllerPlugin() || PluginControllerService.getInstance().reload(this, true, null) == null) {
                        throw new PluginControllerNotFoundException("Plugin controller could not be found for reload: " + getId());
                    }
                }
            } catch (PluginControllerException e) {
                // Store exception for later handling
                mReloadException = e;
            }
        }

        /**
         * Saves the serialized plugin JAR to a temporary directory.
         * @return {@code true} if de-serialization was successful, {@code false} if no plugin is stored with the project.
         * @throws PluginControllerException
         */
        private boolean deSerializeControllerPlugin() throws PluginControllerException {
            if (mSerializedPluginFileMap != null && !mSerializedPluginFileMap.isEmpty()) {
                PluginControllerService.getInstance().saveSerializedPluginFile(mSerializedPluginFileMap);
                return true;
            } else {
                sLog.warning("No plugin files stored with the project.");
                return false;
            }
        }

        @Override
        public String toString() {
            return "PluginControllerDefinition [controllerName=" + this.getName() + ", id="
                + this.getId() + ", pluginJar=" + this.getPluginJarName() + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            PluginControllerDefinition other = (PluginControllerDefinition) obj;
            if (this.id == null) {
                if (other.id != null) {
                    return false;
                }
            } else if (!this.id.equals(other.id)) {
                return false;
            }
            return true;
        }
    }
}
