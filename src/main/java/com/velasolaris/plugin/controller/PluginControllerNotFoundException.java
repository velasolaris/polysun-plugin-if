package com.velasolaris.plugin.controller;

import com.velasolaris.plugin.controller.spi.PluginControllerException;

/**
 * <b>This exception is used by Polysun and should not be thrown by {@code PluginController}s.</b>
 * Thrown when a plugin controller cannot be found (anymore).
 *
 * @author Roland Kurmann
 * @since Polysun 9.1
 *
 */
public class PluginControllerNotFoundException extends PluginControllerException {

    private static final long serialVersionUID = 1L;

    public PluginControllerNotFoundException() {
    }

    public PluginControllerNotFoundException(String aMessage) {
        super(aMessage);
    }

    public PluginControllerNotFoundException(Throwable aCause) {
        super(aCause);
    }

    public PluginControllerNotFoundException(String aMessage, Throwable aCause) {
        super(aMessage, aCause);
    }

    public PluginControllerNotFoundException(String aMessage, Throwable aCause, boolean aEnableSuppression,
            boolean aWritableStackTrace) {
        super(aMessage, aCause, aEnableSuppression, aWritableStackTrace);
    }

}
